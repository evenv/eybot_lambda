# EYbot on Amazon Lambda (using Zappa)

EYbot is a simple slack bot responding to slash commands. The bot itself is configured to lookup Norwegian companies using the brreg.no service.

## Architecture

EYbot is a [Flask](flask.pocoo.org) app using [requests](docs.python-requests.org/en/master/) for querying the brreg.no website and [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/) for HTML parsing. 
It can be run locally using docker and on Amazon Lambda using the very promising [zappa](https://github.com/Miserlou/Zappa).

## Usage

To run locally: Install docker, then run `docker-compose up`.

To run in Lambda: Set up your AWS account, add your AWS credentials in an .env file in the app folder, then run `docker-compose run web zappa deploy dev`.

You can then query <localhost or server>/revisor using slack slash POST syntax (all you need is to add text=<company name> to your POST request.