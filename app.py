import json
import urllib
import random

import requests
from bs4 import BeautifulSoup


from flask import Flask
from flask import jsonify
from flask import request
app = Flask(__name__)

@app.route('/')
def index():
    return jsonify({"status":"EYbot ready to serve"})

@app.route('/revisor', methods=['POST'])
def revisor():
    channel_name = request.form.get('channel_name')
    user_name    = request.form.get('user_name')
    search_text  = request.form.get('text')

    if search_text == None:
        return jsonify({"text":"Selskapsnavnet mangler"})

    filter = urllib.quote("startswith(navn, '{}') and registrertIMvaregisteret eq 'J' and (organisasjonsform eq 'AS' or organisasjonsform eq 'ASA')".format(search_text.strip()))
    content = json.loads(requests.get("http://data.brreg.no/enhetsregisteret/enhet.json?size=5&$filter="+ filter).text)

    if 'data' not in content:
        return slack_response("Ingen selskap funnet (NB: revisorbot finner kun norske selskaper registrert i MVA-registreret)")
    orgnr = content['data'][0]['organisasjonsnummer']
    navn = content['data'][0]['navn']

    doc = requests.get("https://w2.brreg.no/enhet/sok/detalj.jsp?orgnr={}".format(orgnr)).text
    soup = BeautifulSoup(doc, 'html.parser')
    revisortag = soup.find("b",string="Revisor:")
    if revisortag == None:
        return slack_response("{} ({}) funnet men ingen revisor registrert".format(navn,orgnr))

    revisor = revisortag.find_parent("div").find_next_sibling("div").find_all("p")[2].text
    emoji_good = ['moneybag', 'laughing', 'champagne', 'beer', 'sunglasses']
    emoji_bad  = ['cactus', 'rage', 'skull', 'japanese_ogre', 'white_frowning_face', 'cry']
    emoji_list = emoji_bad if revisor == 'ERNST & YOUNG AS' else emoji_good
    emoji = random.choice(emoji_list)
    return slack_response("{} ({}) har *{}* som revisor :{}:".format(navn,orgnr,revisor,emoji))

def slack_response(text):
    return jsonify({"response_type": "in_channel", "text": text})

if __name__ == '__main__':
    app.run(debug = True, host='0.0.0.0', port=80)
